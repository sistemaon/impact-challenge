
# impact-challenge

### Installation Requirements ###
* You need to have to have Node.JS in your computer installed, if you dont, access https://nodejs.org/en/ and follow instructions.

### Runing the project impact-api ###
1. Open your terminal. (However you like).
2. Clone this repository.
3. Access repository directory impact-api.
4. Install node packages to execute and run project. (npm install).
5. Start project, pm2. (npm start)
6. Stop project, pm2. (npm run stop)
7. Delete project, pm2. (npm run delete)
8. Enjoy.

```
cd impact-api
npm install
npm start

* stop pm2, impact-api project
npm run stop

* delte pm2, impact-api project
npm run delete
```

### Runing the project impact-ui ###
1. Open your terminal. (However you like).
2. Clone this repository.
3. Access repository directory impact-ui.
4. Install node packages to execute and run project. (npm install).
5. Start project, live-server. (npm start)
5. Enjoy.

```
cd impact-ui
npm install
npm start
```

> Please note, install and run impact-api first, before installing and running impact-ui
