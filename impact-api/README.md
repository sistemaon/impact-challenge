
# impact-api

### Runing the project impact-api ###
1. Open your terminal. (However you like).
2. Clone this repository.
3. Access repository directory impact-api.
4. Install node packages to execute and run project. (npm install).
5. Start project, pm2. (npm start)
6. Stop project, pm2. (npm run stop)
7. Delete project, pm2. (npm run delete)
8. Enjoy.

```
cd impact-api
npm install
npm start

* stop pm2, impact-api project
npm run stop

* delte pm2, impact-api project
npm run delete
```

> Please note, install and run impact-api first, before installing and running impact-ui
