module.exports = {
  apps : [{
    name: 'IMPACT-API',
    script: './bin/www',

    // Options reference: https://pm2.io/doc/en/runtime/reference/ecosystem-file/
    args: 'impact api',
    instances: 2,
    autorestart: true,
    watch: true,
    max_memory_restart: '4G',
    env: {
      NODE_ENV: 'development'
    },
    env_production: {
      NODE_ENV: 'production'
    }
  }]
};
