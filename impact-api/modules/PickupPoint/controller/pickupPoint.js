
// pickupPoint model
const PickupPoint = require('../model/pickupPoint');

// function do show all pickup points
const showPickupPoints = (req, res, next) => {
  res.status(200).json(PickupPoint.dataJsonFile())
}

// function do show pickup points thru id
const showPickupPointsId = (req, res, next) => {
  const id = req.params.id
  res.status(200).json(PickupPoint.dataJsonFileId(id))
}

// function do show pickup points thru countryCode and postalCode
const showPickupPointsCountryZip = (req, res, next) => {
  const countryCode = req.params.countryCode
  const postalCode = req.params.postalCode
  res.status(200).json(PickupPoint.dataJsonFileCountryZip(countryCode, postalCode))
}

// functions object module to export 
const objModulesToExport = {
	showPickupPoints,
  showPickupPointsId,
  showPickupPointsCountryZip
}

module.exports = objModulesToExport
