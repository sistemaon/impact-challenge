
// Pickup point Json File
const pickupPointsFile = require('../../../pickupPoints');

// function to read all data in file
const dataJsonFile = () => {
  try {
    const file = pickupPointsFile;
    // console.log('dataJsonFile.file ::; ', file);
    return file

  } catch (e) {
    // console.log('dataJsonFile.e ::; ', e);
    throw e
  }
}

// function to read data and filter in file thru id
const dataJsonFileId = (id) => {
  try {
    const file = pickupPointsFile.pickupPoint;
    // console.log('dataJsonFileId.file ::; ', file);
    const fileMap = file.map(data => data);
    // console.log('dataJsonFileId.fileMap ::; ', fileMap);
    const fileFilter = fileMap.filter(data => data.id == id);
    // console.log('dataJsonFileId.fileFilter ::; ', fileFilter);
    return fileFilter;

  } catch (e) {
    // console.log('dataJsonFileId.e ::; ', e);
    throw e
  }
}

// function to read data and filter in file thru countryCode and postalCode, transforming in object
const dataJsonFileCountryZip = (countryCode, postalCode) => {
  try {
    const file = pickupPointsFile.pickupPoint;
    // console.log('dataJsonFileCountryZip.file ::; ', file);
    const fileMap = file.map(data => data);
    // console.log('dataJsonFileCountryZip.fileMap ::; ', fileMap);
    const fileFilter = fileMap.filter(data => data.countryCode == countryCode && data.postalCode == postalCode);
    // console.log('dataJsonFileCountryZip.fileFilter ::; ', fileFilter);
    const fileReduce = fileFilter.reduce( (acc, cur) => {
     acc[cur] = cur
     return acc
    })
    // console.log('dataJsonFileCountryZip.fileReduce ::; ', fileReduce);
    return fileReduce;

  } catch (e) {
    // console.log('dataJsonFileCountryZip.e ::; ', e);
    throw e
  }
}

// functions object module to export
const objModulesToExport = {
  dataJsonFile,
  dataJsonFileId,
  dataJsonFileCountryZip
}

module.exports = objModulesToExport;
