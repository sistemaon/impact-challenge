
const express = require('express');
const router = express.Router();

// pickupPoint controller
const PickupPoint = require('../controller/pickupPoint');

// route to get, show all pickup points
router.get('/searchPickupPoints', PickupPoint.showPickupPoints);

// route to get, show pickup points thru id passing in params
router.get('/searchPickupPoints/:id', PickupPoint.showPickupPointsId);

// route to get, show pickup points thru countryCode and postalCode passing in params
router.get('/searchPickupPoints/:countryCode/postalCode/:postalCode', PickupPoint.showPickupPointsCountryZip);

module.exports = router
