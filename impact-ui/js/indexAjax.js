
// option to reset, pickup false because no pickup location were queried
const MAP = {
  pickup: false
}

// event click
const useClick = `click`

// function get element by its id
const elementId = (id) => document.getElementById(id)

// function add event listener to el
const addEventTo = (el, event, run) =>
  el.addEventListener(event, run)

// error handlers
const ERRORS = [ '',
  (x) =>
    x.innerHTML = "User denied the request for Geolocation.",
  (x) =>
    x.innerHTML = "Location information is unavailable.",
  (x) =>
    x.innerHTML = "The request to get user location timed out."
]

// function to get value from element with the id
const getValueElementId = (id) => elementId(id).value

// function to get its value by pickup
const getValueIdDomain = (id) => getValueElementId(id)

// function to get pickup location by element id pickup from map
const forGetDomainLocation = () => getPickupDomain(getValueIdDomain(`country`), getValueIdDomain(`pickup`))

// function to get own location from map
const forGetOwnLocation = () => getOnwLocation()

// function add event to button to get pickup location
const buttonDomainLocation = addEventTo(elementId(`pickupLocation`),
                                            useClick,
                                            forGetDomainLocation)

// function add event to button to get own location
const buttonOwnLocation = addEventTo(elementId(`ownLocation`),
                                            useClick,
                                            forGetOwnLocation)

// function to get data and parse to json
const getData = (data) => JSON.parse(data)

// function to get google map by its element id
const getGoogleMap = (el, options) =>
  new google.maps.Map(elementId(el), options)

// function to get zoom, if its pickup location or own location
const getZoom = (type) => (type === `own`) ? 17 : 13

// function get options from map
const getMapOptions = (response, type) => ({
  zoom:  getZoom(type),
  center: getPositionCenter(response),
  scrollwheel: false
})

// function to get center position from latitude and longitude
const getPositionCenter = (response) =>
  ({ lat: response.lat, lng: response.lon })

// function to add marker into position in map
const addMarker = (title, map, position) =>
  new google.maps.Marker({
    map,
    position,
    title
  })

// function to add marker in map
const showMapWithMarker = (title, type) => (response) =>
  addMarker( title,
              getGoogleMap('map', getMapOptions(response, type)),
              getPositionCenter(response))

// function to get data from url passed
const successPickup = (title, type) => (data) => {
  const result = getData(data)
  // scroll view
  window.scrollTo(0, 600)

 // function to show map with marker on pickup location
  MAP.pickup = () =>
    showMapWithMarker(title, type)({ lat: result.latitude, lon: result.longitude })
// return marker in pickup location
  return MAP.pickup()
}

// function to show error if pickup is required
const showErrorNotDomain = (value) =>
  alert(`Domain is required! Value: ${value} is invalid or pickup not founded!`)

// function to show error from geolocation
const showErroGeolocation = (err) =>
  ERRORS[ err.code ](elementId(`coordinates`),
                      err.message)

// function to get pickup url from input
const getPickupDomain = (country, url) => {
  // ajax requesition
  const request = new XMLHttpRequest()
  // ajax open, to GET data from api, url as input search, option true async
  request.open(`GET`, `http://localhost:3000/api/searchPickupPoints/${country}/postalCode/${url}`, true)
  // ajax on load, to load data if status between 200 and 399 and diferent of fail
  request.onload = () => {
    (request.status == 200 &&
      // request.status < 400 &&
      JSON.parse(request.responseText).status != 'fail')
        ? successPickup(url)(request.responseText)
        : showErrorNotDomain(url)

        pickupPositionLog(request);
  }
  // console.log('request ::; ', request);
  request.send()
}

// function to get the current position
const getCurrentPositionFrom = (geolocation, showOwnLocation, showErroGeolocation) => {
  return geolocation.getCurrentPosition(showOwnLocation, showErroGeolocation)
}

// function to log the current pickup position
const pickupPositionLog = (position) => {
  console.log('Current Domain Position: ')
  console.log('Latitude : ' + JSON.parse(position.responseText).latitude)
  console.log('Longitude: ' + JSON.parse(position.responseText).longitude)
  // console.log('pickupPositionLog ::; ', position);
}

// function to log position from google map
const ownPositionLog = (position) => {
  console.log('Current Own Position: ')
  console.log('Latitude : ' + position.latitude)
  console.log('Longitude: ' + position.longitude)
  console.log('Accuracy position: ' + position.accuracy)
  // console.log('ownPositionLog() ::; ', position);
}

// function to show own location from own position
const showOwnLocation = (pos) => {
  const position = pos.coords
  window.scrollTo(0, 600)
  // log in console of its own position
  ownPositionLog(position)

  return showMapWithMarker (`Own`)
                              ({ lat: position.latitude,
                                  lon: position.longitude })
}

// function to get own location from current
const getOnwLocation = () =>
  getCurrentPositionFrom(navigator.geolocation,
                          showOwnLocation,
                          showErroGeolocation)
