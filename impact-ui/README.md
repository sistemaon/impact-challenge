
# impact-ui

### Runing the project impact-ui ###
1. Open your terminal. (However you like).
2. Clone this repository.
3. Access repository directory impact-ui.
4. Install node packages to execute and run project. (npm install).
5. Start project, live-server. (npm start)
5. Enjoy.

```
cd impact-ui
npm install
npm start
```

> Please note, install and run impact-api first, before installing and running impact-ui
